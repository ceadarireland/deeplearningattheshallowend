# Deep Learning at the Shallow End: Malware Classification for Non-Domain Experts

This repository contains the code for the deep learning models presented in the DFRWS 2018 proceedings paper "Deep Learning at the Shallow End: Malware Classification for Non-Domain Experts"
https://doi.org/10.1016/j.diin.2018.04.024

## License

[Apache License 2.0](LICENSE)
